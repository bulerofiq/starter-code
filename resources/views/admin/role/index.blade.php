@extends('layouts.app')

@section('title', 'Role')

@section('content')

    @if (session()->has('pesan'))
        <div class="alert alert-success alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>×</span>
                </button>
                {{ session()->get('pesan') }}
            </div>
        </div>
    @endif

    <section class="section">
        <div class="section-header">
            <h1>Management users - role</h1>
        </div>

        <div class="section-body">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ route('role.create') }}" class="btn btn-icon icon-left btn-primary"><i
                                    class="fas fa-plus"></i> Create</a>
                        </div>
                        <div class="card-body p-0">
                            <table class="table table-hover table-md     ml-4">
                                <tr>
                                    <th width="5%">No</th>
                                    <th>Nama</th>
                                    <th>Role</th>
                                    <th>Aksi</th>
                                </tr>

                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($roles as $role)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $role->name }}</td>
                                        <td>
                                            @foreach ($role->getAllPermissions() as $item)
                                                {{ $item->name }} <br>
                                            @endforeach
                                        </td>
                                        <td>
                                            <a href="{{ route('role.edit', $role->id) }}"
                                                class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                                            <form action="{{ route('role.destroy', $role->id) }}"
                                                method="POST" style="display: contents">
                                                @csrf
                                                @method('DELETE')

                                                <button type="submit" class="btn btn-danger btn-sm"
                                                    onclick="return hapus()"><i class="fas fa-trash"></i></button>
                                            </form>
                                            <a href="{{ route('role.setup', $role->id) }}" class="btn btn-primary btn-sm"
                                                data-toggle="tooltip" data-placement="top" title="Setup user"><i
                                                    class="fas fa-user-cog"></i></a>

                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
@stop

@push('js')

    <script>
        function hapus() {
            if (confirm('Are you sure you want to delete this data? Data that has been deleted cannot be recovered !!!')) {
                return true;
            } else {
                return false;
            }
        }
    </script>
@endpush
