@extends('layouts.app')

@section('title', 'Setup permission role')

@section('content_header')
    <h1>Setup permission role</h1>
@stop

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Role - Setup role</h1>
        </div>

        <div class="section-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('role.setup.store') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $role->id }}">
                                @foreach ($arPermissions as $group => $list)
                                    <h4>{{ $group }}</h3>
                                        @foreach ($list as $permission)
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="permission[]"
                                                    value="{{ $permission }}"
                                                    @if ($role->hasPermissionTo($permission)) checked @endif>
                                                <label class="form-check-label">
                                                    {{ $permission }}
                                                </label>
                                            </div>
                                        @endforeach
                                        <br><br>
                                @endforeach
                                <button class="btn btn-success" type="submit">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
@stop