@extends('layouts.app')

@section('title', 'Edit Role')

@section('content_header')
    <h1>Role</h1>
@stop

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Role - Edit role</h1>
        </div>

        <div class="section-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('role.update', $role->id) }}" method="POST">
                                @csrf
                                @method('PATCH')
                                <div class="row">
                                    <div class="col">
                                        <input type="text" class="form-control" name="name" id="name"
                                            value="{{ $role->name }}" autofocus>
                                    </div>
                                    <div class="col">
                                        <button type="submit" class="btn btn-success">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
@stop

@section('css')

@stop

@section('js')

@stop
