@extends('layouts.app')

@section('title', 'Permissions')

@section('content')

    @if (session()->has('pesan'))
        <div class="alert alert-success alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>×</span>
                </button>
                {{ session()->get('pesan') }}
            </div>
        </div>
    @endif

    <section class="section">
        <div class="section-header">
            <h1>Management users - permission</h1>
        </div>

        <div class="section-body">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            {{-- <h4>Advanced Table</h4> --}}
                            <a href="{{ route('permission.create') }}" class="btn btn-icon icon-left btn-primary"><i
                                    class="fas fa-plus"></i> Create</a>
                            {{-- <div class="card-header-form">
                            <form>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div> --}}
                        </div>
                        <div class="card-body p-0">
                            {{-- <div class="table-responsive"> --}}
                            <table class="table table-hover table-md     ml-4">
                                <tr>
                                    {{-- <th width="5%">
                                            <div class="custom-checkbox custom-control">
                                                <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad"
                                                    class="custom-control-input" id="checkbox-all">
                                                <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                                            </div>
                                        </th> --}}
                                    <th width="5%">No</th>
                                    <th>Nama</th>
                                    <th>Aksi</th>
                                </tr>

                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($permissions as $permission)
                                    <tr>
                                        {{-- <td></td> --}}
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $permission->name }}</td>
                                        <td>
                                            <a href="{{ route('permission.edit', $permission->id) }}"
                                                class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                                            <form action="{{ route('permission.destroy', $permission->id) }}" method="POST"
                                                style="display: contents">
                                                @csrf
                                                @method('DELETE')

                                                <button type="submit" class="btn btn-danger btn-sm"
                                                    onclick="return hapus()"><i class="fas fa-trash"></i></button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            {{-- </div> --}}
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>

@stop

@push('js')

    <script>
        function hapus() {
            if (confirm('Are you sure you want to delete this data? Data that has been deleted cannot be recovered !!!')) {
                return true;
            } else {
                return false;
            }
        }
    </script>
@endpush