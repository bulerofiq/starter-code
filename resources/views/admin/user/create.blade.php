@extends('layouts.app')

@section('title', 'Tambah User')

@section('content_header')
    <h1>User</h1>
@stop

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>User - Create user</h1>
        </div>

        <div class="section-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('user.store') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" name="name" id="name" autofocus>
                                    </div>
                                    <div class="col">
                                        <label for="name">Username</label>
                                        <input type="text" class="form-control" name="username" id="username" autofocus>
                                    </div>
                                    <div class="col">
                                        <label for="name">Password</label>
                                        <input type="password" class="form-control" name="password" id="password"
                                            autofocus>
                                    </div>
                                    <div class="col">
                                        <button type="submit" class="btn btn-success mt-4">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
@stop

@section('css')

@stop

@section('js')

@stop
