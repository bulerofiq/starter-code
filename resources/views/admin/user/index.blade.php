@extends('layouts.app')

@section('title', 'User')

@section('content')

    @if (session()->has('pesan'))
        <div class="alert alert-success alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>×</span>
                </button>
                {{ session()->get('pesan') }}
            </div>
        </div>
    @endif

    <section class="section">
        <div class="section-header">
            <h1>Management users - user</h1>
        </div>

        <div class="section-body">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ route('user.create') }}" class="btn btn-icon icon-left btn-primary"><i
                                    class="fas fa-plus"></i> Create</a>
                        </div>
                        <div class="card-body p-0">
                            <table class="table table-hover table-md ml-4">
                                <tr>
                                    <th width="5%">No</th>
                                    <th>Nama</th>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Department</th>
                                    <th>Aksi</th>
                                </tr>

                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->username }}</td>
                                        <td>
                                            @if ($user->roles)
                                                @foreach ($user->roles as $role)
                                                    {{ $role->name }} <br>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            @forelse ($user->userDepartment as $userDepartmentItem)
                                                {{ $userDepartmentItem->department->name }} <br>
                                            @empty
                                            @endforelse
                                        </td>
                                        <td>
                                            <a href="{{ route('user.edit', $user->id) }}" class="btn btn-warning btn-sm"><i
                                                    class="fas fa-edit" data-toggle="tooltip" data-placement="top" title="Edit user"></i></a>
                                            <form action="{{ route('user.destroy', $user->id) }}" method="POST"
                                                style="display: contents">
                                                @csrf
                                                @method('DELETE')

                                                <button type="submit" class="btn btn-danger btn-sm"
                                                    onclick="return hapus()"><i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="Delete user"></i></button>
                                            </form>
                                            <a href="{{ route('user.setup', $user->id) }}" class="btn btn-primary btn-sm"
                                                data-toggle="tooltip" data-placement="top" title="Setup user"><i
                                                    class="fas fa-user-cog"></i></a>
                                            <a href="{{ route('user.setup.department', $user->id) }}" class="btn btn-primary btn-sm"
                                                data-toggle="tooltip" data-placement="top" title="Setup user department"><i
                                                    class="fas fa-building"></i></a>

                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>

@stop

@section('css')

@stop

@section('js')

    <script>
        function hapus() {
            if (confirm('Apa anda yakin akan menghapus data ini? Data yang sudah dihapus tidak dpaat dikembalikan !!!')) {
                return true;
            } else {
                return false;
            }
        }
    </script>
@stop
