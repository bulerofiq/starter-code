@extends('layouts.app')

@section('title', 'Setup user department')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Setup user department</h1>
        </div>

        <div class="section-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('user.setup.department.store') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                @foreach ($departments as $department)
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="department[{{ $department->id }}]"
                                            value="{{ $department->name }}" @isset ($userDepartment[$department->id]) checked @endisset>
                                        <label class="form-check-label">
                                            {{ $department->name }}
                                        </label>
                                    </div>
                                @endforeach
                                <br>
                                <button class="btn btn-success" type="submit">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
@stop
