@extends('layouts.app')

@section('title', 'Department')

@section('content')

    @if (session()->has('pesan'))
        <div class="alert alert-success alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>×</span>
                </button>
                {{ session()->get('pesan') }}
            </div>
        </div>
    @endif

    <section class="section">
        <div class="section-header">
            <h1>Departments</h1>
        </div>

        <div class="section-body">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ route('department.create') }}" class="btn btn-icon icon-left btn-primary"><i
                                    class="fas fa-plus"></i> Create</a>
                        </div>
                        <div class="card-body p-0">
                            <table class="table table-hover table-md ml-4">
                                <tr>
                                    <th width="5%">No</th>
                                    <th>Nama</th>
                                    <th>Aksi</th>
                                </tr>

                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($departments as $department)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $department->name }}</td>
                                        <td>
                                            <a href="{{ route('department.edit', $department->id) }}" class="btn btn-warning btn-sm"><i
                                                    class="fas fa-edit"></i></a>
                                            <form action="{{ route('department.destroy', $department->id) }}" method="POST"
                                                style="display: contents">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-sm"
                                                    onclick="return hapus()"><i class="fas fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>

@stop

@section('js')
    <script>
        function hapus() {
            if (confirm('Apa anda yakin akan menghapus data ini? Data yang sudah dihapus tidak dpaat dikembalikan !!!')) {
                return true;
            } else {
                return false;
            }
        }
    </script>
@stop
