<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <a href="index.html">Survey</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <a href="index.html">SRV</a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">Dashboard</li>
        <li class="nav-item">
            <a href="{{ route('home') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        <li class="menu-header">Setting</li>
        <li class="nav-item dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>Management Users</span></a>
            <ul class="dropdown-menu">
                @if('Read User')
                <li>
                    <a class="nav-link" href="{{ route('user.index') }}"><i class="fas fa-user"></i> Users</a>
                </li>
                @endcan
                @can('Read Permission')
                <li>
                    <a class="nav-link" href="{{ route('permission.index') }}"><i class="fas fa-fw fa-user-shield"></i>Permissions</a>
                </li>
                @endcan
                @can('Read Role')
                <li>
                    <a class="nav-link" href="{{ route('role.index') }}"><i class="fas fa-fw fa-user-circle"></i>Role</a>
                </li>
                @endcan
            </ul>
        </li>
        <li class="nav-item">
            <a href="{{ route('department.index') }}" class="nav-link"><i class="fas fa-building"></i><span>Deparment</span></a>
        </li>
    </ul>
</aside>