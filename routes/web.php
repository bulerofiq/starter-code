<?php

use App\Http\Controllers\Admin\DepartmentController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\RoleSetupController;
use App\Http\Controllers\Admin\SurveyController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\UserSetupController;
use GuzzleHttp\Middleware;
use Illuminate\Http\Request;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Route::middleware(['auth'])->group(function () {
    Route::get('home', function () {
        return view('dashboard.home');
    })->name('home');

    Route::prefix('admin')->group(function () {
        Route::resource('user', UserController::class);
        Route::get('user/setup/{id}', [UserSetupController::class, 'index'])->name('user.setup');
        Route::post('user/setup', [UserSetupController::class, 'store'])->name('user.setup.store');
        Route::resource('permission', PermissionController::class);
        Route::resource('role', RoleController::class)->except(['show']);
        Route::get('role/setup/{id}', [RoleSetupController::class, 'index'])->name('role.setup');
        Route::post('role/setup', [RoleSetupController::class, 'store'])->name('role.setup.store');
        Route::resource('department', DepartmentController::class);
        Route::get('user/setup/department/{id}', [UserSetupController::class, 'indexDepartment'])->name('user.setup.department');
        Route::post('user/setup/department', [UserSetupController::class, 'storeDepartment'])->name('user.setup.department.store');
    });
});
