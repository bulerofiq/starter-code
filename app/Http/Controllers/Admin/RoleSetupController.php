<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSetupController extends Controller
{
    public function index($id)
    {
        $role = Role::find($id);
        $permissions = Permission::all();
        $arPermissions = array();
        foreach ($permissions as $key => $value) {
            $slice = explode(" ", $value->name);
            if (count($slice) == 2) {
                $arPermissions[$slice[1]][] = $value->name;
            } else {
                $arPermissions['lain-lain'][] = $value->name;
            }
        }

        return view('admin.role.setup', compact('role', 'permissions', 'arPermissions'));
    }

    public function store(Request $request)
    {
        $role = Role::find($request->id);
        if (isset($request->permission)) {
            $permissions = Permission::whereIn('name', $request->permission)->get();
            $role->syncPermissions($permissions);
        } else {
            $role->syncPermissions();
        }
        return redirect('admin/role')->with('pesan', 'Setup role berhasil');
    }
}
