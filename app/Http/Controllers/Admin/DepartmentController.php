<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    public function index()
    {
        $departments = Department::orderBy('name')->get();
        return view('admin.department.index', compact('departments'));
    }

    public function create()
    {
        return view('admin.department.create');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
        ]);

        Department::create([
            'name' => $request->name,
            'created_by' => auth()->user()->id,
        ]);
        
        return redirect('admin/department')->with('pesan', 'Data has been saved !');
    }

    public function edit($id)
    {
        $department = department::find($id);
        
        return view('admin.department.edit', compact('department'));
    }
    
    public function update($id, Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
        ]);
        
        Department::whereId($id)->update([
            'name' => $request->name,
            'updated_by' => auth()->user()->id,
        ]);

        return redirect('admin/department')->with('pesan', 'Data has been changed !');
    }

    public function destroy($id)
    {
        Department::whereId($id)->delete();

        return redirect('admin/department')->with('pesan', 'Data has been deleted !');
    }
}
