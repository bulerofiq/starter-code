<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\User;
use App\Models\UserDepartment;
use App\Models\Users;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserSetupController extends Controller
{
    public function index($id)
    {
        $user = User::find($id);
        $roles = Role::all();

        return view('admin.user.setup', compact('user', 'roles'));
    }

    public function store(Request $request)
    {
        $user = User::find($request->id);
        if (isset($request->role)) {
            $roles = Role::whereIn('name', $request->role)->get();
            $user->syncRoles($roles);
        } else {
            $user->syncRoles();
        }
        return redirect('admin/user')->with('pesan', 'Setup role user has been saved');
    }

    public function indexDepartment($id)
    {
        $user = Users::with(['hasDepartment'])->find($id);
        $userDepartment = UserDepartment::where('user_id', $user->id)->get()->keyBy('department_id');
        $departments = Department::all();

        return view('admin.user.setup-department', compact('user', 'userDepartment', 'departments'));
    }

    public function storeDepartment(Request $request)
    {
        $user_id = $request->user_id;
        if ($request->department) {
            foreach($request->department as $department_id => $department_name) {
                UserDepartment::updateOrInsert(
                    ['user_id' => $user_id, 'department_id' => $department_id],
                    ['created_by' => auth()->user()->id]
                );
            }
        } else {
            UserDepartment::where('user_id', $user_id)->delete();
        }

        return redirect('admin/user')->with('pesan', 'Setup department user has been saved');
    }
}
