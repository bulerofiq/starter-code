<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function index()
    {
        $permissions = Permission::orderBy('name')->get();
        return view('admin.permission.index', compact('permissions'));
    }

    public function create()
    {
        return view('admin.permission.create');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
        ]);

        Permission::create(['name' => $request->name]);

        return redirect('admin/permission')->with('pesan', 'Data has been saved !');
    }

    public function edit($id)
    {
        $permission = Permission::find($id);

        return view('admin.permission.edit', compact('permission'));
    }

    public function update($id, Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
        ]);

        Permission::whereId($id)->update(['name' => $request->name]);

        return redirect('admin/permission')->with('pesan', 'Data has been changed !');
    }

    public function destroy($id)
    {
        Permission::destroy($id);

        return redirect('admin/permission')->with('pesan', 'Data has been deleted !');
    }
}
