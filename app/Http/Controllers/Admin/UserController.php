<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index()
    {
        // dd(auth()->user()->roles);
        // $users = User::with('unit')->orderBy('name')->get();
        $users = User::orderBy('name')->get();
        return view('admin.user.index', compact('users'));
    }

    public function create()
    {
        // $unit = Unit::all();
        return view('admin.user.create');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'username' => 'required',
            'password' => 'required',
            // 'unit_id' => 'required',
        ]);

        DB::table('users')->insert([
            'name' => $request->name,
            'username' => $request->username,
            'password' => bcrypt($request->password),
            // 'unit_id' => $request->unit_id,
        ]);

        return redirect('admin/user')->with('pesan', 'Data has been saved !');
    }

    public function edit($id)
    {
        $user = User::find($id);
        // $unit = Unit::all();

        return view('admin.user.edit', compact('user'));
    }

    public function update($id, Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'username' => 'required',
            // 'unit_id' => 'required',
        ]);

        if ($request->password != null) {
            $password = bcrypt($request->password);
        } else {
            $password = User::find($id)->password;
        }

        User::whereId($id)->update([
            'name' => $request->name,
            'username' => $request->username,
            'password' => $password,
            // 'unit_id' => $request->unit_id,
        ]);

        return redirect('admin/user')->with('pesan', 'Data has been changed !');
    }

    public function destroy($id)
    {
        User::whereId($id)->delete();

        return redirect('admin/user')->with('pesan', 'Data has been deleted !');
    }
}
