<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function __construct()
    {
        if ( ! auth()->user()->can('Read Role')) {
            abort(403);
        }
    }

    public function index()
    {
        $roles = Role::orderBy('name')->get();
        return view('admin.role.index', compact('roles'));
    }

    public function create()
    {
        return view('admin.role.create');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
        ]);

        Role::create(['name' => $request->name]);

        return redirect('admin/role')->with('pesan', 'Data has been saved !');
    }

    public function edit($id)
    {
        $role = Role::find($id);

        return view('admin.role.edit', compact('role'));
    }

    public function update($id, Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
        ]);

        Role::whereId($id)->update(['name' => $request->name]);

        return redirect('admin/role')->with('pesan', 'Data has been changed !');
    }

    public function destroy($id)
    {
        Role::destroy($id);

        return redirect('admin/role')->with('pesan', 'Data has been deleted !');
    }
}
