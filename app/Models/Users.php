<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    use HasFactory;

    protected $table = 'users';

    public function hasDepartment()
    {
        return $this->hasOne(UserDepartment::class, 'user_id', 'id');
    }
}
